/*
Pet Finder app project by Pet Squad
week10 on 7/18/19
Web Development class at Goodwill University
Student: Daniel "Danny" K. King
React app: psquad
Rev# k 8/4/19 app.js
*/
import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Navigation from './shared/navigation/Navigation';
// import MainBody from './page/MainBody';
import Homepage2 from './page/home/Homepage';
import Homepage from './page/home/Homepage2';
import Article1 from './page/Article1b';
// import Article2 from './page/Article2b';
import About from "./page/About";
import Contact from "./page/Contact";
import Error from "./page/Error";
import Footer from './shared/footer/Footer';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
// end import statements
// begin App function
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <div>
          <Navigation />
          <Switch>
            <Route path="/" component={Homepage} exact />
            <Route path="/" component={Homepage2} exact />
            <Route path="/article1b" component={Article1} />
            {/* <Route path="/article2b" component={Article2} /> */}
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <Route component={Error} />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
