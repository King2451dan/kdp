import React, { Component } from 'react';
import Card from '../../shared/card/Card';
import ShoppingCart from '../../shared/shopping-cart/ShoppingCart';

class ProductCatalog extends Component {
    constructor(){
        super();
        this.state = {
            products : [
                {
                    id : 1,
                    imageSrc : 'https://picsum.photos/200',
                    name : 'Bag',
                    price : '$10.99'
                },
                {
                    id : 2,
                    imageSrc : 'https://picsum.photos/200',
                    name : 'Book',
                    price : '$1.99'
                },
                {
                    id : 3,
                    imageSrc : 'https://picsum.photos/200',
                    name : 'Shoe',
                    price : '$100.00'
                },
                {
                    id : 4,
                    imageSrc : 'https://picsum.photos/200',
                    name : 'Stickers',
                    price : '$0.99'
                },
                {
                    id : 5,
                    imageSrc : 'https://picsum.photos/200',
                    name : 'Glue',
                    price : '$1.29'
                }
            ],
            selectedProducts : [
                {
                    id : 1,
                    imageSrc : 'https://picsum.photos/200',
                    name : 'Bag',
                    price : '$10.99'
                },
                {
                    id : 2,
                    imageSrc : 'https://picsum.photos/200',
                    name : 'Book',
                    price : '$1.99'
                }
            ]
        }
        this.handleCartChange = this.handleCartChange.bind(this);
    }

    render() {
        return (
        <main className="row">
            <section className="col-md-8">
                <div className="card-columns">
                    {
                        this.state.products.map((product)=>{
                            return <Card key={product.id} imageSrc={product.imageSrc} name={product.name} price={product.price} valueChange={this.handleCartChange}/>
                        })
                    }
                </div>
            </section>
            <aside className="col-md-4">
                <ShoppingCart itemsInCart={this.state.selectedProducts}/>
            </aside>
        </main>
        );
    }

    handleCartChange(newSelectedItem){
        console.log('handle cart change', newSelectedItem);
        const newSelectedProducts = this.state.selectedProducts;
        newSelectedProducts.push(newSelectedItem);

        this.setState({selectedProducts : newSelectedProducts});
    }
}

export default ProductCatalog;