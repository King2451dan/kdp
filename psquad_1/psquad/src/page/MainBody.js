import React, { Component } from 'react';

class MainBody extends Component {
    render() {
        return (
            <main className="row">
                {this.props.children}
            </main>
        );
    }
}

export default MainBody;