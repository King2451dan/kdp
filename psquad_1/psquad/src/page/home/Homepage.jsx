/*
Pet Finder app project by Pet Squad
week10 on 7/18/19
Web Development class at Goodwill University
Student: Daniel "Danny" K. King
React app: psquad
Rev# j 8/4/19 Homepage.jsx
*/
import React, { Component } from 'react';
class Homepage extends Component {
  render() {
    return (
      <div>
        {/* Required meta tags */}
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Pet Squad</title>
        <link rel="stylesheet" type="text/css" href="homepage.css" />

        {/* Begin body of Lashonda's homepage.html */}
        <div><h3>Success Stories</h3>
        </div>
        {/* Page Content */}
        <div className="container">
          <div className="row">
            <div className="col-sm-3">
              <div className="card">
                <img className="card-img-top" src="images/cuddle time.jfif" alt="a young girl and her pet cat Libby" />
                                <div className="card-body text-center">
                  <h5 className="card-title">Libby</h5>
                  <p className="card-text text-left">Pet Squad, thank you for helping me find Libby! Libby is a vital part of our
                    family, you guys were with us every step of the way until Libby was returned.
                    again we can't thank you guys enough! Eternally grateful, Harper!</p>
                  <p className="card-text"><small className="text-muted">January 02, 2019</small></p>
                </div>
              </div>
            </div>
            <div className="col-sm-3">
              <div className="card">
                <img className="card-img-top" src="images/new dog.jfif" alt="a small young black dog with brown paws" />
                <div className="card-body text-center">
                  <h5 className="card-title">Chase</h5>
                  <p className="card-text text-left"> After 16 days, Lula was found and taken to the vet for care.
                    She was dehydrated and underweight and her paws were damaged but, otherwise,
                    she is ok! We're so happy she's back in our lives! Pet Squad ROCKS!</p>
                  <p className="card-text"><small className="text-muted">August 20, 2018</small></p>
                </div>
              </div>
            </div>
            <div className="col-sm-3">
              <div className="card">
                <img className="card-img-top" src="images/servicedog.jpg" alt="solider with his companion a German Sherpard" />
                <div className="card-body text-center">
                  <h5 className="card-title text-center">Gippy</h5>
                  <p className="card-text text-left">I woke up this morning know Gippy was right next to me, was the best Christmas
                    present by far. Pet Squad services brought Gippy home, thank you, Pet Squad!!!
                    -Andy from Charlotte, NC</p>
                  <p className="card-text"><small className="text-muted">December 25, 2005</small></p>
                </div>
              </div>
            </div>
            <div className="col-sm-3">
              <div className="card">
                <img className="card-img-top" src="images/new pig.jfif" alt="a full grown hog pictured with owner an two dogs" />
                <div className="card-body text-center">
                  <h5 className="card-title">Kehlani</h5>
                  <p className="card-text text-left" /><p className="card-text">Kehlani (our pet pig) was lost to us during a storm. She is very smart but couldn't
                    find her way back home due to downed trees and some flooding. We reached out to Pet Squad, within a week
                    our beloved Kehlani was located through the services of Pet Squad. Thank you Pet Squad for brining her
                    back to us!
                    -Stevie</p>
                  <p className="card-text"><small className="text-muted">July 12, 2002</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*  
JSON driven navigation bar
Media embedding of pet photos / Image cards driven by JSON, default on load 
Information sign in form for user and pet info and upload photo feature sign in so information is available to other areas
Contact Us - (Form )
Location of local pet shelters google map
Alerts
Create Lost pet Flyer
(Possible future feature) Chat Now
*/}
        
        {/* Load our React component. */}
        {/* Header, Navbar, import carousel from './src/carousel', footer*/}
      </div>
    );
  }
}
export default Homepage;