import React, { Component } from 'react';

class OrderHistory extends Component {
    constructor() {
        super();
        this.state = {
            orderList: [{
                id: 123,
                orderDate: '07/23/2019',
                orderTotal: '$103.99',
                orderStatus: 'Processing'
            },
            {
                id: 133,
                orderDate: '07/22/2019',
                orderTotal: '$213.99',
                orderStatus: 'Processing'
            },
            {
                id: 423,
                orderDate: '07/21/2019',
                orderTotal: '$88.99',
                orderStatus: 'Processing'
            },
            {
                id: 523,
                orderDate: '07/25/2019',
                orderTotal: '$893.99',
                orderStatus: 'Processing'
            }]
        }
    }

    render() {
        return (
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Order Date</th>
                            <th scope="col">Total</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.orderList.map((order, index) => {
                                return <tr key={index}>
                                    <th scope="row">{order.id}</th>
                                    <td>{order.orderDate}</td>
                                    <td>{order.orderTotal}</td>
                                    <td>{order.orderStatus}</td>
                                </tr>
                            })
                        }

                    </tbody>
                </table>
            </div>
        );
    }
}

export default OrderHistory;