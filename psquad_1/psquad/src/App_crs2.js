// from Classrom practice
import React from 'react';
import './App.css';
import Navigation from './shared/navigation/Navigation';
import Footer from './shared/footer/Footer';
import OrderPage from './page/order/OrderPage';
import ProductCatalog from './page/order/ProductCatalog';
import OrderHistory from './page/order-history/OrderHistory';
import { BrowserRouter, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div>
        <Navigation />
        <OrderPage>
          <Route path="/order" component={ProductCatalog}/>
          <Route path="/history" component={OrderHistory}/>
          </OrderPage>
        <Footer />
      </div>
    </BrowserRouter>
  );

}

export default App;
