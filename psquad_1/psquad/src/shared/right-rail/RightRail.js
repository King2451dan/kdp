import React, { Component } from 'react';
import ShoppingCart from '../shopping-cart/ShoppingCart';

class RightRail extends Component {
    render() {
        return (
            <aside className={this.props.column}>
                <ShoppingCart/>
            </aside>
        );
    }
}

export default RightRail;