/*
Pet Finder app project by Pet Squad
week10 on 7/18/19
Web Development class at Goodwill University
Student: Daniel "Danny" K. King
React app: psquad
Rev# k 8/4/19 Navigation.js
*/
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Navigation extends Component {
    /*navigation bar  menu-links Home, Registration, Lost Pets, Found Pets */
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark static-top">
                    <div className="container-fluid">
                        <a className="navbar-brand" href="#" />
                        {/* Logo in public/images folder */}
                        <img src='images/logo2.jpg' alt='pfour dog paws that make a circle with a small red cross in the middle' width={100} height={175} />
                        {/*React puck up this: srcSet='logo21.JPG 1x, logo21@2x.JPG 2x' */}
                        {/* end logo2 */}

                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon" />
                        </button>
                        <NavLink className="nav-link" activeClassName="active" to="/">Home</NavLink>
                        <NavLink className="nav-link" activeClassName="active" to="../Article1b">Article1b</NavLink>
                        {/* <NavLink className="nav-link" activeClassName="active" to="../Article2b">Article2b</NavLink> */}
                        <NavLink className="nav-link" activeClassName="active" to="../About">About</NavLink>
                        <NavLink className="nav-link" activeClassName="active" to="../Contact">Contact</NavLink>
                    </div>
                </nav>
            </div>
        );
    }
}
export default Navigation;