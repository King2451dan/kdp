import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Navigation extends Component {
    constructor() {
        super();
        this.state = {
            theme: 'light'
        };
        this.handleThemeChange = this.handleThemeChange.bind(this);
    }

    render() {
        return (
            <header className={`navbar navbar-expand-lg navbar-light bg-${this.state.theme}`}>
                <nav className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link active" href="#">Classroom</a>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" activeClassName="active" to="/order">New Order</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" activeClassName="active" to="/history">Order History</NavLink>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Settings</a>
                        </li>
                        <li>
                            <select id="themechanger" onChange={this.handleThemeChange}>
                                <option value="light">Light</option>
                                <option value="success">Primary</option>
                                <option value="danger">Error</option>
                            </select>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }

    handleThemeChange() {
        const selectedTheme = document.getElementById('themechanger').value;
        this.setState({ theme: selectedTheme });
    }
}

export default Navigation_crs2;