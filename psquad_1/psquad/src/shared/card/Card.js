import React, { Component } from 'react';

class Card extends Component {
    constructor(){
        super();
        this.handleAddToCart = this.handleAddToCart.bind(this);
    }
    
    render() {
        return (
            <div className="card">
                <img src={this.props.imageSrc} className="card-img-top" alt={this.props.name}/>
                <div className="card-body">
                    <h5 className="card-title">{this.props.name}</h5>
                    <p className="card-text">{this.props.price}</p>
                    <button type="button" className="btn btn-primary" onClick={this.handleAddToCart}>Add To Cart</button>
                </div>
            </div>
        );
    }

    handleAddToCart(){
        const newProduct = {
            id : this.props.id,
            name : this.props.name,
            price : this.props.price,
            imageSrc : this.props.imageSrc
        };

        this.props.valueChange(newProduct);
    }
}

export default Card;