import React, { Component } from 'react';
{/* Lashonda's home page footer 
Converted by Danny King
Rev# m 8/4/19 Footer.jsx
*/}
class Footer extends Component {
    render() {
        const currentYear = (new Date()).getFullYear();
        return (
            <div>
                <div className="card-group">
                    <div className="card-footer bg-dark text-white">
                        {/* Content */}
                        <div className="card-body text-left">
                            <h5 className="text-uppercase">Privacy Policy</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#">We collect information from and about you.</a>
                                </li>
                                <li>
                                    <a href="#">We collect information in different ways.</a>
                                </li>
                                <li>
                                    <a href="#">Sharing of your information with thrid parties.</a>
                                </li>
                                <li>
                                    <a href="#">Your information is secure.</a>
                                </li>
                            </ul></div>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Contact Us</a>
                        </li>
                    </div>
                    {/*add another card body*/}
                    <div>
                        <a className="btn btn-just-icon btn-round btn-twitter" href="https://twitter.com/">
                            <i className="fa fa-twitter" />
                        </a>
                        <a className="btn btn-just-icon btn-round btn-facebook" href="https://www.facebook.com/">
                            <i className="fa fa-facebook" />
                        </a>
                        <a className="btn btn-just-icon btn-round btn-google" href="https://www.google.com/">
                            <i className="fa fa-google" />
                        </a>
                    </div>

                    &copy; Copyright {currentYear} PetSquad app
                </div>
                </div>
                );
            }
        }
   export default Footer;