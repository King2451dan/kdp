import React, { Component } from 'react';

class ShoppingCart extends Component {
    render() {

        return (
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.itemsInCart.map((item, index)=>{
                            return <tr key={index}>
                            <th scope="row">{index+1}</th>
                            <td>{item.name}</td>
                            <td>{item.price}</td>
                        </tr>
                        })
                    }
                    
                </tbody>
            </table>
        );
    }
}

export default ShoppingCart;